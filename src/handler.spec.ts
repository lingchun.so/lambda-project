process.env.S3_ENDPOINT = process.env.S3_ENDPOINT ?? 'http://localhost:4566';
process.env.BUCKET_NAME = 'local-bucket';
process.env.S3_FORCE_PATH_STYLE = 'true';
process.env.AWS_ACCESS_KEY_ID = 'test';
process.env.AWS_SECRET_ACCESS_KEY = 'test';

import { handler } from './handler';
import { S3 } from 'aws-sdk';
import { ALBEvent } from 'aws-lambda';

describe('Handler test integration', () => {
  let s3Client: S3;

  beforeAll(async () => {
    s3Client = new S3({ endpoint: process.env.S3_ENDPOINT, s3ForcePathStyle: !!process.env.S3_FORCE_PATH_STYLE });
    await s3Client
      .createBucket({
        Bucket: 'local-bucket',
      })
      .promise();
  });

  it('should generate return presigned url', () => {
    const result = handler({ body: JSON.stringify({ author: 'Tripod Studio', title: 'Lost Ark' }) } as ALBEvent);
    expect(result.statusCode).toEqual(200);
    const body = JSON.parse(result.body as string);
    expect(body.presignedUrl).toContain(`${process.env.S3_ENDPOINT}/local-bucket/bucket/path`);
    expect(body.presignedUrl).toContain('x-amz-meta-title=Lost%20Ark');
    expect(body.presignedUrl).toContain('x-amz-meta-author=Tripod%20Studio');
  });
});
