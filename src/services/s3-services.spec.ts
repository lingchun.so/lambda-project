/* eslint-disable @typescript-eslint/no-empty-function */
import { S3 } from 'aws-sdk';
import { S3Service } from './s3-services';
import * as uuid from 'uuid';

jest.mock('uuid');

const s3ClientMock = {
  getSignedUrl: () => {},
} as unknown as S3;

describe('S3 service unit tests', () => {
  const s3Service = new S3Service(s3ClientMock);

  it('should call presigned url with correct payload and metadata', () => {
    const presignedUrl =
      'https://bucket.s3.region.amazonaws.com/myfile.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=random-aws-credential-to-identify-the-signer&X-Amz-Date=timestamp-of-generation-of-url&X-Amz-Expires=validity-from-generation-timestamp&X-Amz-Signature=6ffca338-f5b2-48ad-89ec-4ae462cb46dc&X-Amz-SignedHeaders=host';
    jest.spyOn(s3ClientMock, 'getSignedUrl').mockReturnValue(presignedUrl);
    jest.spyOn(uuid, 'v4').mockReturnValue('11aa4c90-fb02-445d-b898-da7f4bbaadd3');

    const result = s3Service.generatePresignedUrl(
      { request_id: '123126af-2ce6-41f2', author: 'Pikachu' },
      'local-bucket',
    );

    expect(s3ClientMock.getSignedUrl).toHaveBeenCalledWith('putObject', {
      Bucket: 'local-bucket',
      Key: 'bucket/path/11aa4c90-fb02-445d-b898-da7f4bbaadd3.txt',
      Metadata: {
        request_id: '123126af-2ce6-41f2',
        author: 'Pikachu',
      },
    });

    expect(result).toEqual(presignedUrl);
  });
});
