import { S3 } from 'aws-sdk';
import { v4 as uuidv4 } from 'uuid';

export class S3Service {
  private s3Client: S3;

  constructor(s3Client: S3) {
    this.s3Client = s3Client;
  }

  generatePresignedUrl(metadata: unknown, bucketName: string): string {
    const params = {
      Bucket: bucketName,
      Key: `bucket/path/${uuidv4()}.txt`,
      Metadata: metadata,
    };

    return this.s3Client.getSignedUrl('putObject', params);
  }
}
