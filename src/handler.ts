import { S3 } from 'aws-sdk';
import { ALBEvent, ALBResult } from 'aws-lambda';
import { S3Service } from './services/s3-services';

const S3_CLIENT = new S3({ endpoint: process.env.S3_ENDPOINT, s3ForcePathStyle: !!process.env.S3_FORCE_PATH_STYLE });
const S3_SERVICE = new S3Service(S3_CLIENT);

export function handler(event: ALBEvent): ALBResult {
  const metadata = JSON.parse(event.body as string);
  const presignedUrl = S3_SERVICE.generatePresignedUrl(metadata, process.env.BUCKET_NAME as string);

  return {
    statusCode: 200,
    body: JSON.stringify({
      presignedUrl,
    }),
  };
}
