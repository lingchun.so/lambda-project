/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const ZipPlugin = require('zip-webpack-plugin');

module.exports = {
  entry: './src/handler.ts',
  target: 'node',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  output: {
    filename: 'lambda.js',
    libraryTarget: 'commonjs',
    path: path.resolve(__dirname, 'dist'),
  },
  plugins: [new ZipPlugin({ filename: 'lambda.zip' })],
};
